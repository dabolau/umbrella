package controller

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"

	"gitee.com/dabolau/umbrella/common"
	"gitee.com/dabolau/umbrella/database"
	"gitee.com/dabolau/umbrella/model"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// ResponseBill 响应数据
type ResponseBill struct {
	Title        string
	Description  string
	Button       string
	Message      string
	Fullname     string
	Username     string
	Bills        []model.Bill
	Bill         model.Bill
	ID           string
	Text         string
	Gte          string
	Lte          string
	Page         int
	PreviousPage int
	NextPage     int
	MinPage      int
	MaxPage      int
	TotalPage    int
	Year         string
}

// BillHandler 票据信息
func BillHandler(c *gin.Context) {
	var message string
	var text string
	var gte string
	var lte string
	var obj *gorm.DB
	var page = 1
	var pageSize = 10
	var totalSize int64
	var bills []model.Bill
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		text = c.Query("text")
		gte = c.Query("gte")
		lte = c.Query("lte")
		page, _ = strconv.Atoi(c.Query("page"))
		if page <= 0 {
			page = 1
		}
		// 查询数据
		if gte == "" && lte == "" {
			obj = database.DB.Model(&bills).Where("fullname LIKE ?", fmt.Sprintf("%%%v%%", text)).Or("umbrella_name LIKE ?", fmt.Sprintf("%%%v%%", text)).Order("ID DESC")
		} else {
			obj = database.DB.Model(&bills).Where("created_at > ? AND created_at < ? AND fullname LIKE ?", fmt.Sprintf("%s 00:00:00", gte), fmt.Sprintf("%s 23:59:59", lte), fmt.Sprintf("%%%s%%", text)).Or("created_at > ? AND created_at < ? AND umbrella_name LIKE ?", fmt.Sprintf("%s 00:00:00", gte), fmt.Sprintf("%s 23:59:59", lte), fmt.Sprintf("%%%s%%", text)).Order("ID DESC")
		}
		obj.Count(&totalSize)
		obj.Offset((page - 1) * pageSize).Limit(pageSize).Find(&bills)
	case "POST":
		log.Println("POST")
	}
	// 显示网页
	c.HTML(http.StatusOK, "bill/bill.html", ResponseBill{
		Title:        "票据信息",
		Description:  "",
		Button:       "票据信息",
		Message:      message,
		Fullname:     c.MustGet("fullname").(string),
		Username:     c.MustGet("username").(string),
		Bills:        bills,
		Text:         text,
		Gte:          gte,
		Lte:          lte,
		Page:         page,
		PreviousPage: page - 1,
		NextPage:     page + 1,
		MinPage:      1,
		MaxPage:      int(math.Ceil(float64(totalSize) / float64(pageSize))),
		Year:         common.GetYear(),
	})
}

// BillDetailHandler 票据详情
func BillDetailHandler(c *gin.Context) {
	var message string
	var id string
	var bill model.Bill
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&bill, id)
	case "POST":
		log.Println("POST")
	}
	// 显示网页
	c.HTML(http.StatusOK, "bill/detail.html", ResponseBill{
		Title:       "票据详情",
		Description: "",
		Button:      "票据详情",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		Bill:        bill,
		ID:          id,
		Year:        common.GetYear(),
	})
}

// BillAddHandler 票据添加
func BillAddHandler(c *gin.Context) {
	var message string
	var requestBill model.Bill
	var bill model.Bill
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
	case "POST":
		log.Println("POST")
		// 绑定前端数据
		c.Bind(&requestBill)
		// 添加语句
		bill = model.Bill{
			Fullname:     requestBill.Fullname,
			UmbrellaName: requestBill.UmbrellaName,
			BorrowAt:     requestBill.BorrowAt,
			ReturnAt:     requestBill.ReturnAt,
		}
		// 添加到数据库
		database.DB.Create(&bill)
		log.Println(bill)
		// 添加成功
		if bill.ID > 0 {
			message = "添加成功"
			// 跳转页面
			c.Redirect(http.StatusFound, "/bill")
			return
		}
		// 添加失败
		message = "添加失败"
	}
	// 显示网页
	c.HTML(http.StatusOK, "bill/add.html", ResponseBill{
		Title:       "票据添加",
		Description: "",
		Button:      "票据添加",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		Year:        common.GetYear(),
	})
}

// BillChangeHandler 票据编辑
func BillChangeHandler(c *gin.Context) {
	var message string
	var id string
	var requestBill model.Bill
	var bill model.Bill
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&bill, id)
	case "POST":
		log.Println("POST")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&bill, id)
		// 绑定前端数据
		c.Bind(&requestBill)
		// 更新数据
		database.DB.Model(&bill).Updates(model.Bill{
			Fullname:     requestBill.Fullname,
			UmbrellaName: requestBill.UmbrellaName,
			BorrowAt:     requestBill.BorrowAt,
			ReturnAt:     requestBill.ReturnAt,
			DifferenceAt: requestBill.DifferenceAt,
		})
		// 更新成功
		if bill.ID > 0 {
			message = "更新成功"
			// 跳转页面
			c.Redirect(http.StatusFound, "/bill")
			return
		}
		// 更新失败
		message = "更新失败"
	}
	// 显示网页
	c.HTML(http.StatusOK, "bill/change.html", ResponseBill{
		Title:       "票据编辑",
		Description: "",
		Button:      "票据编辑",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		Bill:        bill,
		ID:          id,
		Year:        common.GetYear(),
	})
}

// BillDeleteHandler 票据删除
func BillDeleteHandler(c *gin.Context) {
	var message string
	var id string
	var bill model.Bill
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
	case "POST":
		log.Println("POST")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&bill, id)
		log.Println(bill)
		// 删除数据库
		database.DB.Delete(&bill)
		log.Println(bill.ID)
		// 删除成功
		if bill.ID > 0 {
			message = "删除成功"
			// 跳转页面
			c.Redirect(http.StatusFound, "/bill")
			return
		}
		// 删除失败
		message = "删除失败"
	}
	// 显示网页
	c.HTML(http.StatusOK, "bill/delete.html", ResponseBill{
		Title:       "票据删除",
		Description: "",
		Button:      "票据删除",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		ID:          id,
		Year:        common.GetYear(),
	})
}

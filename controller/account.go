package controller

import (
	"fmt"
	"log"
	"net/http"

	"gitee.com/dabolau/umbrella/common"
	"gitee.com/dabolau/umbrella/database"
	"gitee.com/dabolau/umbrella/middleware"
	"gitee.com/dabolau/umbrella/model"

	"github.com/gin-gonic/gin"
)

// ResponseAccount 响应数据
type ResponseAccount struct {
	Title       string
	Description string
	Button      string
	Message     string
	Year        string
}

// AccountHandler 帐号信息
func AccountHandler(c *gin.Context) {
	c.Redirect(http.StatusFound, "/account/login")
}

// LoginHandler 登录帐号
func LoginHandler(c *gin.Context) {
	var message string
	var account model.User
	var requestAccount model.User
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
	case "POST":
		log.Println("POST")
		// 绑定前端数据
		c.Bind(&requestAccount)
		// 查询数据
		database.DB.Where(&model.User{
			Username: requestAccount.Username,
			Password: common.EncodeBase64(requestAccount.Password),
		}).Or(&model.User{
			Email:    requestAccount.Username,
			Password: common.EncodeBase64(requestAccount.Password),
		}).First(&account)
		// 当用户和密码或者邮件和密码有其中一个存在时即可成功登录
		if account.ID > 0 {
			// 设置Cookie
			// cookieToken := middleware.GenerateCookie(account.Username, common.DecodeBase64(account.Password))
			// middleware.SetCookie(c, "cookieToken", cookieToken)
			// 设置Session
			// sessionToken := middleware.GenerateSession(account.Username, common.DecodeBase64(account.Password))
			// middleware.SetSession(c, "sessionToken", sessionToken)
			// 设置Jwt
			jwtToken, _ := middleware.GenerateJwt(account.Username, common.DecodeBase64(account.Password))
			middleware.SetJwt(c, "jwtToken", jwtToken)
			// 登录成功
			message = "登录成功"
			// 页面跳转
			c.Redirect(http.StatusFound, "/dashboard")
			return
		}
		// 登录失败
		message = "登录失败"
	}
	// 显示网页
	c.HTML(http.StatusOK, "account/login.html", ResponseAccount{
		Title:       "登录帐号",
		Description: "",
		Button:      "登录",
		Message:     message,
		Year:        common.GetYear(),
	})
}

// RegisterHandler 注册帐号
func RegisterHandler(c *gin.Context) {
	var message string
	var account model.User
	var requestAccount model.User
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
	case "POST":
		log.Println("POST")
		// 绑定前端数据
		c.Bind(&requestAccount)
		// 查询数据
		database.DB.Where(&model.User{
			Username: requestAccount.Username,
		}).Or(&model.User{
			Email: requestAccount.Email,
		}).First(&account)
		// 当用户和邮箱都不存在时即可成功注册
		if account.ID < 1 {
			// 添加到数据库
			database.DB.Create(&model.User{
				Username: requestAccount.Username,
				Email:    requestAccount.Email,
				Password: common.EncodeBase64(requestAccount.Password),
			})
			// 注册成功
			message = "注册成功"
		} else {
			// 注册失败
			message = "注册失败"
		}
	}
	// 显示网页
	c.HTML(http.StatusOK, "account/register.html", ResponseAccount{
		Title:       "注册帐号",
		Description: "",
		Button:      "注册",
		Message:     message,
		Year:        common.GetYear(),
	})
}

// ChangePasswordHandler 修改密码
func ChangePasswordHandler(c *gin.Context) {
	var message string
	var account model.User
	var requestAccount model.User
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
	case "POST":
		log.Println("POST")
		// 绑定前端数据
		c.Bind(&requestAccount)
		// 获取表单数据
		newPassword := c.PostForm("new_password")
		confirmPassword := c.PostForm("confirm_password")
		// 查询数据
		database.DB.Where(&model.User{
			Username: requestAccount.Username,
			Password: common.EncodeBase64(requestAccount.Password),
		}).First(&account)
		// 当用户和密码存在时即可修改密码
		if account.ID > 0 {
			if newPassword == confirmPassword {
				account.Password = common.EncodeBase64(newPassword)
				database.DB.Save(&account)
				// 修改成功
				message = "修改成功"
			} else {
				// 修改失败
				message = "修改失败"
			}
		} else {
			// 修改失败
			message = "修改失败"
		}
	}
	c.HTML(http.StatusOK, "account/changepassword.html", ResponseAccount{
		Title:       "修改密码",
		Description: "",
		Button:      "修改密码",
		Message:     message,
		Year:        common.GetYear(),
	})
}

// GetPasswordHandler 找回密码
func GetPasswordHandler(c *gin.Context) {
	var message string
	var account model.User
	var requestAccount model.User
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
	case "POST":
		log.Println("POST")
		// 绑定前端数据
		c.Bind(&requestAccount)
		// 查询数据
		database.DB.Where(&model.User{Email: requestAccount.Email}).First(&account)
		// 当邮箱存在时发送邮件到指定邮箱
		if account.ID > 0 {
			// 发送文本邮件
			// err := common.SendTextEMail(account.Email, "找到您的信息", fmt.Sprintf("您的账号：[%v]，密码：[%v]，请及时修改。", account.Username, common.DecodeBase64(account.Password)))
			// 发送超文本标记邮件
			err := common.SendHTMLEMail(account.Email, "找到您的信息", fmt.Sprintf(common.HTMLEMailTemplate, account.Username, common.DecodeBase64(account.Password)))
			if err != nil {
				// 找回失败，打印发送邮件遇到的错误
				message = "找回失败"
				log.Println(err)
			} else {
				// 找回成功
				message = "找回成功"
			}
		} else {
			// 找回失败
			message = "找回失败"
		}
	}
	c.HTML(http.StatusOK, "account/getpassword.html", ResponseAccount{
		Title:       "找回密码",
		Description: "",
		Button:      "找回密码",
		Message:     message,
		Year:        common.GetYear(),
	})
}

// LogoutHandler 注销帐号
func LogoutHandler(c *gin.Context) {
	var message string
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 删除Cookie
		// middleware.DeleteCookie(c, "cookieToken")
		// 删除Session
		// middleware.DeleteSession(c, "sessionToken")
		// 删除jwt
		middleware.DeleteJwt(c, "jwtToken")
		// 页面跳转
		c.Redirect(http.StatusFound, "/account/login")
		return
	case "POST":
		log.Println("POST")
	}
	c.HTML(http.StatusOK, "account/logout.html", ResponseAccount{
		Title:       "注销帐号",
		Description: "",
		Button:      "注销帐号",
		Message:     message,
		Year:        common.GetYear(),
	})
}

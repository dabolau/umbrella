package controller

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"gitee.com/dabolau/umbrella/common"
	"gitee.com/dabolau/umbrella/database"
	"gitee.com/dabolau/umbrella/model"

	"github.com/gin-gonic/gin"
)

// ResponseHome 响应数据
type ResponseHome struct {
	Title         string
	Description   string
	Button        string
	Message       string
	Fullname      string
	Username      string
	ID            string
	UserCount     int64
	BillCount     int64
	UmbrellaCount int64
	Year          string
}

// NotFoundHandler 找不到页面
func NotFoundHandler(c *gin.Context) {
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
	case "POST":
		log.Println("POST")
	case "PUT":
		log.Println("PUT")
	case "DELETE":
		log.Println("DELETE")
	}
	// 显示网页
	c.HTML(http.StatusNotFound, "home/404.html", nil)
}

// HomeHandler 网站首页
func HomeHandler(c *gin.Context) {
	// 创建二维码
	content := fmt.Sprintf("%v%v", "http://", c.Request.Host)
	common.CreateQRCodeFile(content, "static/qrcode/home/home.png")
	// 显示网页
	c.HTML(http.StatusOK, "home/home.html", ResponseHome{
		Title:       "共享雨伞",
		Description: "共享雨伞 随时随地 随借随还",
		Button:      "进入我的仪表盘",
		Message:     "",
		Year:        common.GetYear(),
	})
}

// // HomeHandler 网站首页
// func HomeHandler(c *gin.Context) {
// 	c.Redirect(http.StatusFound, "/account/login")
// }

// DashboardHandler 我的仪表盘
func DashboardHandler(c *gin.Context) {
	var userCount int64
	var users []model.User
	var billCount int64
	var bills []model.Bill
	var umbrellaCount int64
	var umbrellas []model.Umbrella
	// 查询数据，获取用户，票据，雨伞的数据数
	database.DB.Find(&users).Count(&userCount)
	database.DB.Find(&bills).Count(&billCount)
	database.DB.Find(&umbrellas).Count(&umbrellaCount)
	//////////////////////
	var t1 = "2020-11-01 15:15:05"
	var t2 = "2020-11-03 15:16:09"
	// var t2 = "2020-12-01 16:09:09"
	log.Println("时间", t1, t2)

	difHourString := common.GetHoursDifference(t1, t2)
	log.Println(difHourString, "时")

	difMinuteString := common.GetMinutesDifference(t1, t2)
	log.Println(difMinuteString, "分")

	difSecondString := common.GetSecondsDifference(t1, t2)
	log.Println(difSecondString, "秒")

	dif := common.GetHourMinuteSecondDifference(t1, t2)
	log.Println(dif)

	/////////////////////
	// 显示网页
	c.HTML(http.StatusOK, "home/dashboard.html", ResponseHome{
		Title:         "我的仪表盘",
		Description:   "",
		Button:        "",
		Message:       "",
		Fullname:      c.MustGet("fullname").(string), // 在请求上下文中获取值，需在中间件中设置
		Username:      c.MustGet("username").(string), // 在请求上下文中获取值，需在中间件中设置
		UserCount:     userCount,
		BillCount:     billCount,
		UmbrellaCount: umbrellaCount,
		Year:          common.GetYear(),
	})
}

// BugHandler 问题反馈
func BugHandler(c *gin.Context) {
	// 显示网页
	c.HTML(http.StatusOK, "home/bug.html", ResponseHome{
		Title:       "问题反馈",
		Description: "",
		Button:      "进入我的仪表盘",
		Message:     "",
		Year:        common.GetYear(),
	})
}

// IndexHandler 用户中心
func IndexHandler(c *gin.Context) {
	var description string
	var message string
	var button string
	var umbrella model.Umbrella
	// 在请求上下文中获取值，需在中间件中设置
	var username = c.MustGet("username").(string)
	var fullname = c.MustGet("fullname").(string)
	log.Println("获取上下文中的数据", username, fullname)
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 查询数据
		database.DB.Where(&model.Umbrella{
			Username: username,
		}).First(&umbrella)
		// 使用状态
		if umbrella.ID > 0 {
			description = "找到记录，请用微信或浏览器扫码归还"
			message = "找到记录"
			button = fmt.Sprintf("找到记录，您正在使用的雨伞是（%v）", strconv.Itoa(int(umbrella.ID)))
		} else {
			description = "暂无记录，请用微信或浏览器扫码借用"
			message = "暂无记录"
			button = "暂无记录"
		}
	case "POST":
		log.Println("POST")
	}
	// 显示网页
	c.HTML(http.StatusOK, "home/index.html", ResponseHome{
		Title:       "个人中心",
		Description: description,
		Message:     message,
		Button:      button,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		Year:        common.GetYear(),
	})
}

// BorrowHandler 借用雨伞
func BorrowHandler(c *gin.Context) {
	var description string
	var message string
	var button string
	var id string
	var umbrella model.Umbrella
	var umbrellaByID model.Umbrella
	// 在请求上下文中获取值，需在中间件中设置
	var username = c.MustGet("username").(string)
	var fullname = c.MustGet("fullname").(string)
	log.Println("获取上下文中的数据", username, fullname)
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&umbrellaByID, id)
		// 查询数据
		database.DB.Where(&model.Umbrella{
			Username: username,
		}).First(&umbrella)
		log.Println("#1", umbrellaByID)
		log.Println("#2", umbrella.ID)
		// 使用状态
		if umbrella.ID < 1 && umbrellaByID.Username == "" {
			description = "下雨了，请用微信或浏览器扫码借用"
			message = "允许借用"
			button = "是的，我确定要借用雨伞"
		} else if umbrella.ID < 1 && umbrellaByID.Username != "" {
			description = "拒绝借用，请用微信或浏览器扫码借用"
			message = "拒绝借用，此雨伞已被借用了"
			button = "拒绝借用，此雨伞已被借用了"
		} else {
			description = "拒绝借用，请用微信或浏览器扫码借用"
			message = "拒绝借用，您已借用过雨伞了"
			button = fmt.Sprintf("拒绝借用，您正在使用的雨伞是（%v）", strconv.Itoa(int(umbrella.ID)))
		}
	case "POST":
		log.Println("POST")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&umbrella, id)
		// 更新数据
		database.DB.Model(&umbrella).Updates(model.Umbrella{
			Username: username,
			Fullname: fullname,
			BorrowAt: common.GetYearMonthDayHourMinuteSecond(),
		})
		log.Println("借用时间", umbrella.BorrowAt)
		message = "更新成功"
		// 跳转页面
		c.Redirect(http.StatusFound, "/index")
		return
	}
	// 显示网页
	c.HTML(http.StatusOK, "home/borrow.html", ResponseHome{
		Title:       "借用雨伞",
		Description: description,
		Message:     message,
		Button:      button,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		ID:          id,
		Year:        common.GetYear(),
	})
}

// ReturnHandler 归还雨伞
func ReturnHandler(c *gin.Context) {
	var description string
	var message string
	var button string
	var id string
	var umbrella model.Umbrella
	var bill model.Bill
	// 在请求上下文中获取值，需在中间件中设置
	var username = c.MustGet("username").(string)
	var fullname = c.MustGet("fullname").(string)
	log.Println("获取上下文中的数据", username, fullname)
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.Where(&model.Umbrella{
			Username: username,
		}).First(&umbrella)
		log.Println(umbrella)
		// 使用状态
		if umbrella.ID > 0 && strconv.Itoa(int(umbrella.ID)) == id {
			description = "天晴了，请用微信或浏览器扫码归还"
			message = "允许归还"
			button = "是的，我确定要归还雨伞"
		} else if umbrella.ID > 0 && strconv.Itoa(int(umbrella.ID)) != id {
			description = "拒绝归还，请用微信或浏览器扫码归还"
			message = "拒绝归还"
			button = fmt.Sprintf("拒绝归还，您正在使用的雨伞是（%v）", strconv.Itoa(int(umbrella.ID)))
		} else {
			description = "暂无记录，请用微信或浏览器扫码借用"
			message = "暂无记录"
			button = "暂无记录"
		}
	case "POST":
		log.Println("POST")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&umbrella, id)
		// 获取借用时间和归还时间以及当前时间
		borrowAt := umbrella.BorrowAt
		retrunAt := common.GetYearMonthDayHourMinuteSecond()
		differenceAt := common.GetHourMinuteSecondDifference(borrowAt, retrunAt)
		log.Println("借用时间和当前时间以及时差")
		log.Println(borrowAt, retrunAt, differenceAt)
		// 更新数据
		database.DB.Model(&umbrella).Updates(model.Umbrella{
			Username:     username,
			ReturnAt:     common.GetYearMonthDayHourMinuteSecond(),
			DifferenceAt: differenceAt,
		})
		log.Println("数据库中的数据", umbrella.Username, umbrella.Fullname, umbrella.UmbrellaName, umbrella.BorrowAt, umbrella.ReturnAt, umbrella.DifferenceAt)
		// 添加语句
		bill = model.Bill{
			Fullname:     umbrella.Fullname,
			UmbrellaName: umbrella.UmbrellaName,
			BorrowAt:     umbrella.BorrowAt,
			ReturnAt:     umbrella.ReturnAt,
			DifferenceAt: umbrella.DifferenceAt,
		}
		// 添加到数据库
		database.DB.Create(&bill)
		log.Println(bill)
		// 清空数据，保存数据
		umbrella.Username = ""
		umbrella.Fullname = ""
		umbrella.BorrowAt = ""
		umbrella.ReturnAt = ""
		umbrella.DifferenceAt = ""
		database.DB.Save(umbrella)
		message = "更新成功"
		// 跳转页面
		c.Redirect(http.StatusFound, "/index")
		return
	}
	// 显示网页
	c.HTML(http.StatusOK, "home/return.html", ResponseHome{
		Title:       "归还雨伞",
		Description: description,
		Message:     message,
		Button:      button,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		ID:          id,
		Year:        common.GetYear(),
	})
}

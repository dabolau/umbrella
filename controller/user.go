package controller

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"

	"gitee.com/dabolau/umbrella/common"
	"gitee.com/dabolau/umbrella/database"
	"gitee.com/dabolau/umbrella/model"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// ResponseUser 响应数据
type ResponseUser struct {
	Title          string
	Description    string
	Button         string
	Message        string
	UserID         int
	Fullname       string
	Username       string
	Users          []model.User
	User           model.User
	ID             string
	EncodePassword string
	DecodePassword string
	Text           string
	Gte            string
	Lte            string
	Page           int
	PreviousPage   int
	NextPage       int
	MinPage        int
	MaxPage        int
	TotalPage      int
	Year           string
}

// UserHandler 用户信息
func UserHandler(c *gin.Context) {
	var message string
	var text string
	var gte string
	var lte string
	var obj *gorm.DB
	var page = 1
	var pageSize = 10
	var totalSize int64
	var users []model.User
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		text = c.Query("text")
		gte = c.Query("gte")
		lte = c.Query("lte")
		page, _ = strconv.Atoi(c.Query("page"))
		if page <= 0 {
			page = 1
		}
		// 查询数据
		if gte == "" && lte == "" {
			obj = database.DB.Model(&users).Where("username LIKE ?", fmt.Sprintf("%%%v%%", text)).Or("fullname LIKE ?", fmt.Sprintf("%%%v%%", text)).Order("ID DESC")
		} else {
			obj = database.DB.Model(&users).Where("created_at > ? AND created_at < ? AND username LIKE ?", fmt.Sprintf("%s 00:00:00", gte), fmt.Sprintf("%s 23:59:59", lte), fmt.Sprintf("%%%s%%", text)).Or("created_at > ? AND created_at < ? AND fullname LIKE ?", fmt.Sprintf("%s 00:00:00", gte), fmt.Sprintf("%s 23:59:59", lte), fmt.Sprintf("%%%s%%", text)).Order("ID DESC")
		}
		obj.Count(&totalSize)
		obj.Offset((page - 1) * pageSize).Limit(pageSize).Find(&users)
	case "POST":
		log.Println("POST")
	}
	// 显示网页
	c.HTML(http.StatusOK, "user/user.html", ResponseUser{
		Title:        "用户信息",
		Description:  "",
		Button:       "用户信息",
		Message:      message,
		Fullname:     c.MustGet("fullname").(string),
		Username:     c.MustGet("username").(string),
		Users:        users,
		Text:         text,
		Gte:          gte,
		Lte:          lte,
		Page:         page,
		PreviousPage: page - 1,
		NextPage:     page + 1,
		MinPage:      1,
		MaxPage:      int(math.Ceil(float64(totalSize) / float64(pageSize))),
		Year:         common.GetYear(),
	})
}

// UserDetailHandler 用户详情
func UserDetailHandler(c *gin.Context) {
	var message string
	var id string
	var user model.User
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&user, id)
	case "POST":
		log.Println("POST")
	}
	// 显示网页
	c.HTML(http.StatusOK, "user/detail.html", ResponseUser{
		Title:       "用户详情",
		Description: "",
		Button:      "用户详情",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		User:        user,
		ID:          id,
		Year:        common.GetYear(),
	})
}

// UserAddHandler 用户添加
func UserAddHandler(c *gin.Context) {
	var message string
	var responseUser model.User
	var users []model.User
	var mails []model.User
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
	case "POST":
		log.Println("POST")
		// 绑定前端数据
		c.Bind(&responseUser)
		// 查询数据
		database.DB.Where(&model.User{
			Username: responseUser.Username,
		}).Find(&users)
		database.DB.Where(&model.User{
			Email: responseUser.Email,
		}).Find(&mails)
		// 添加成功
		if len(users) < 1 && len(mails) < 1 {
			// 添加到数据库
			database.DB.Create(&model.User{
				Username: responseUser.Username,
				Email:    responseUser.Email,
				Fullname: responseUser.Fullname,
				Phone:    responseUser.Phone,
				Password: common.EncodeBase64(responseUser.Password),
				Rule:     responseUser.Rule,
			})
			message = "添加成功"
			// 跳转页面
			c.Redirect(http.StatusFound, "/user")
			return
		}
		// 添加失败
		message = "添加失败"
	}
	// 显示网页
	c.HTML(http.StatusOK, "user/add.html", ResponseUser{
		Title:       "用户添加",
		Description: "",
		Button:      "用户添加",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		Year:        common.GetYear(),
	})
}

// UserChangeHandler 用户编辑
func UserChangeHandler(c *gin.Context) {
	var message string
	var id string
	var responseUser model.User
	var user model.User
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&user, id)
	case "POST":
		log.Println("POST")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&user, id)
		// 绑定前端数据
		c.Bind(&responseUser)
		// 更新数据
		database.DB.Model(&user).Updates(model.User{
			Username: responseUser.Username,
			Email:    responseUser.Email,
			Fullname: responseUser.Fullname,
			Phone:    responseUser.Phone,
			Password: common.EncodeBase64(responseUser.Password),
			Rule:     responseUser.Rule,
		})
		message = "更新成功"
		// 跳转页面
		c.Redirect(http.StatusFound, "/user")
		return
	}
	// 显示网页
	c.HTML(http.StatusOK, "user/change.html", ResponseUser{
		Title:          "用户编辑",
		Description:    "",
		Button:         "用户编辑",
		Message:        message,
		Fullname:       c.MustGet("fullname").(string),
		Username:       c.MustGet("username").(string),
		User:           user,
		ID:             id,
		DecodePassword: common.DecodeBase64(user.Password),
		Year:           common.GetYear(),
	})
}

// UserDeleteHandler 用户删除
func UserDeleteHandler(c *gin.Context) {
	var message string
	var id string
	var user model.User
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
	case "POST":
		log.Println("POST")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&user, id)
		// 删除数据库
		database.DB.Delete(&user)
		message = "删除成功"
		// 跳转页面
		c.Redirect(http.StatusFound, "/user")
		return
	}
	// 显示网页
	c.HTML(http.StatusOK, "user/delete.html", ResponseUser{
		Title:       "用户删除",
		Description: "",
		Button:      "用户删除",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		ID:          id,
		Year:        common.GetYear(),
	})
}

// UserProfileHandler 编辑我的信息
func UserProfileHandler(c *gin.Context) {
	var message string
	var username = ""
	var password = ""
	var responseUser model.User
	var user model.User
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 在请求上下文中获取值，需在中间件中设置
		username = c.MustGet("username").(string)
		password = c.MustGet("password").(string)
		log.Println(username, password)
		// 查询数据
		database.DB.Where(&model.User{
			Username: username,
			Password: common.EncodeBase64(password),
		}).First(&user)
	case "POST":
		log.Println("POST")
		// 在请求上下文中获取值，需在中间件中设置
		username = c.MustGet("username").(string)
		password = c.MustGet("password").(string)
		log.Println(username, password)
		// 查询数据
		database.DB.Where(&model.User{
			Username: username,
			Password: common.EncodeBase64(password),
		}).First(&user)
		// 绑定前端数据
		c.Bind(&responseUser)
		// 更新数据
		database.DB.Model(&user).Updates(model.User{
			Fullname: responseUser.Fullname,
			Phone:    responseUser.Phone,
		})
		message = "更新成功"
		// 跳转页面
		c.Redirect(http.StatusFound, "/dashboard")
		return
	}
	// 显示网页
	c.HTML(http.StatusOK, "user/profile.html", ResponseUser{
		Title:       "编辑我的信息",
		Description: "",
		Button:      "编辑我的信息",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		User:        user,
		Year:        common.GetYear(),
	})
}

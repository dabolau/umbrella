package controller

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"

	"gitee.com/dabolau/umbrella/common"
	"gitee.com/dabolau/umbrella/database"
	"gitee.com/dabolau/umbrella/model"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// ResponseUmbrella 响应数据
type ResponseUmbrella struct {
	Title        string
	Description  string
	Button       string
	Message      string
	Fullname     string
	Username     string
	Umbrellas    []model.Umbrella
	Umbrella     model.Umbrella
	ID           string
	Text         string
	Gte          string
	Lte          string
	Page         int
	PreviousPage int
	NextPage     int
	MinPage      int
	MaxPage      int
	TotalPage    int
	Year         string
}

// UmbrellaHandler 雨伞信息
func UmbrellaHandler(c *gin.Context) {
	var message string
	var text string
	var gte string
	var lte string
	var obj *gorm.DB
	var page = 1
	var pageSize = 10
	var totalSize int64
	var umbrellas []model.Umbrella
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		text = c.Query("text")
		gte = c.Query("gte")
		lte = c.Query("lte")
		page, _ = strconv.Atoi(c.Query("page"))
		if page <= 0 {
			page = 1
		}
		// 查询数据
		if gte == "" && lte == "" {
			obj = database.DB.Model(&umbrellas).Where("umbrella_name LIKE ?", fmt.Sprintf("%%%v%%", text)).Or("umbrella_tag LIKE ?", fmt.Sprintf("%%%v%%", text)).Order("ID DESC")
		} else {
			obj = database.DB.Model(&umbrellas).Where("created_at > ? AND created_at < ? AND umbrella_name LIKE ?", fmt.Sprintf("%s 00:00:00", gte), fmt.Sprintf("%s 23:59:59", lte), fmt.Sprintf("%%%s%%", text)).Or("created_at > ? AND created_at < ? AND umbrella_tag LIKE ?", fmt.Sprintf("%s 00:00:00", gte), fmt.Sprintf("%s 23:59:59", lte), fmt.Sprintf("%%%s%%", text)).Order("ID DESC")
		}
		obj.Count(&totalSize)
		obj.Offset((page - 1) * pageSize).Limit(pageSize).Find(&umbrellas)
	case "POST":
		log.Println("POST")
	}
	// 显示网页
	c.HTML(http.StatusOK, "umbrella/umbrella.html", ResponseUmbrella{
		Title:        "雨伞信息",
		Description:  "",
		Button:       "雨伞信息",
		Message:      message,
		Fullname:     c.MustGet("fullname").(string),
		Username:     c.MustGet("username").(string),
		Umbrellas:    umbrellas,
		Text:         text,
		Gte:          gte,
		Lte:          lte,
		Page:         page,
		PreviousPage: page - 1,
		NextPage:     page + 1,
		MinPage:      1,
		MaxPage:      int(math.Ceil(float64(totalSize) / float64(pageSize))),
		Year:         common.GetYear(),
	})
}

// UmbrellaDetailHandler 雨伞详情
func UmbrellaDetailHandler(c *gin.Context) {
	var message string
	var id string
	var umbrella model.Umbrella
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&umbrella, id)
		// 创建借用雨伞二维码
		borrowContent := fmt.Sprintf("%v%v%v%v", "http://", c.Request.Host, "/borrow?id=", umbrella.ID)
		common.CreateQRCodeFile(borrowContent, fmt.Sprintf("static/qrcode/borrow/%v.png", umbrella.ID))
		// 创建归还雨伞二维码
		returnContent := fmt.Sprintf("%v%v%v%v", "http://", c.Request.Host, "/return?id=", umbrella.ID)
		common.CreateQRCodeFile(returnContent, fmt.Sprintf("static/qrcode/return/%v.png", umbrella.ID))
	case "POST":
		log.Println("POST")
	}
	// 显示网页
	c.HTML(http.StatusOK, "umbrella/detail.html", ResponseUmbrella{
		Title:       "雨伞详情",
		Description: "",
		Button:      "雨伞详情",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		Umbrella:    umbrella,
		ID:          id,
		Year:        common.GetYear(),
	})
}

// UmbrellaAddHandler 雨伞添加
func UmbrellaAddHandler(c *gin.Context) {
	var message string
	var requestUmbrella model.Umbrella
	var umbrella model.Umbrella
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
	case "POST":
		log.Println("POST")
		// 绑定前端数据
		c.Bind(&requestUmbrella)
		// 添加语句
		umbrella = model.Umbrella{
			UmbrellaName:        requestUmbrella.UmbrellaName,
			UmbrellaTag:         requestUmbrella.UmbrellaTag,
			UmbrellaDescription: requestUmbrella.UmbrellaDescription,
		}
		// 添加到数据库
		database.DB.Create(&umbrella)
		log.Println(umbrella)
		// 添加成功
		if umbrella.ID > 0 {
			message = "添加成功"
			// 跳转页面
			c.Redirect(http.StatusFound, "/umbrella")
			return
		}
		// 添加失败
		message = "添加失败"
	}
	// 显示网页
	c.HTML(http.StatusOK, "umbrella/add.html", ResponseUmbrella{
		Title:       "雨伞添加",
		Description: "",
		Button:      "雨伞添加",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		Year:        common.GetYear(),
	})
}

// UmbrellaChangeHandler 雨伞编辑
func UmbrellaChangeHandler(c *gin.Context) {
	var message string
	var id string
	var requestUmbrella model.Umbrella
	var umbrella model.Umbrella
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&umbrella, id)
	case "POST":
		log.Println("POST")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&umbrella, id)
		// 绑定前端数据
		c.Bind(&requestUmbrella)
		// 更新数据
		database.DB.Model(&umbrella).Updates(model.Umbrella{
			UmbrellaName:        requestUmbrella.UmbrellaName,
			UmbrellaTag:         requestUmbrella.UmbrellaTag,
			UmbrellaDescription: requestUmbrella.UmbrellaDescription,
		})
		// 更新成功
		if umbrella.ID > 0 {
			message = "更新成功"
			// 跳转页面
			c.Redirect(http.StatusFound, "/umbrella")
			return
		}
		// 更新失败
		message = "更新失败"
	}
	// 显示网页
	c.HTML(http.StatusOK, "umbrella/change.html", ResponseUmbrella{
		Title:       "雨伞编辑",
		Description: "",
		Button:      "雨伞编辑",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		Umbrella:    umbrella,
		ID:          id,
		Year:        common.GetYear(),
	})
}

// UmbrellaDeleteHandler 雨伞删除
func UmbrellaDeleteHandler(c *gin.Context) {
	var message string
	var id string
	var umbrella model.Umbrella
	// 路由使用Any时，就这样区分方法
	switch c.Request.Method {
	case "GET":
		log.Println("GET")
		// 获取参数
		id = c.Query("id")
	case "POST":
		log.Println("POST")
		// 获取参数
		id = c.Query("id")
		// 查询数据
		database.DB.First(&umbrella, id)
		log.Println(umbrella)
		// 删除数据库
		database.DB.Delete(&umbrella)
		log.Println(umbrella.ID)
		// 删除成功
		if umbrella.ID > 0 {
			message = "删除成功"
			// 跳转页面
			c.Redirect(http.StatusFound, "/umbrella")
			return
		}
		// 删除失败
		message = "删除失败"
	}
	// 显示网页
	c.HTML(http.StatusOK, "umbrella/delete.html", ResponseUmbrella{
		Title:       "雨伞删除",
		Description: "",
		Button:      "雨伞删除",
		Message:     message,
		Username:    c.MustGet("username").(string),
		Fullname:    c.MustGet("fullname").(string),
		ID:          id,
		Year:        common.GetYear(),
	})
}

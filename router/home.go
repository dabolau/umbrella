package router

import (
	"gitee.com/dabolau/umbrella/controller"
	"gitee.com/dabolau/umbrella/middleware"

	"github.com/gin-gonic/gin"
)

// Home 首页路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func Home(r *gin.Engine) {
	// 找不到页面
	r.NoRoute(controller.NotFoundHandler)
	// 首页路由
	r.GET("/", controller.HomeHandler)
	// 我的仪表盘
	r.GET("/dashboard", middleware.AuthJwt(), middleware.AuthRule(), middleware.AuthName(), controller.DashboardHandler)
	// 问题反馈
	r.GET("/bug", controller.BugHandler)
	// 个人中心
	r.Any("/index", middleware.AuthJwt(), middleware.AuthName(), controller.IndexHandler)
	// 借用雨伞
	r.Any("/borrow", middleware.AuthJwt(), middleware.AuthName(), controller.BorrowHandler)
	// 归还雨伞
	r.Any("/return", middleware.AuthJwt(), middleware.AuthName(), controller.ReturnHandler)
}

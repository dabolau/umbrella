package router

import (
	"gitee.com/dabolau/umbrella/controller"
	"gitee.com/dabolau/umbrella/middleware"

	"github.com/gin-gonic/gin"
)

// Umbrella 雨伞路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func Umbrella(r *gin.Engine) {
	// 雨伞分组
	umbrellaGroup := r.Group("/umbrella")
	// 注册组中间件
	umbrellaGroup.Use(middleware.AuthJwt(), middleware.AuthRule())
	{
		// 雨伞信息
		umbrellaGroup.Any("", controller.UmbrellaHandler)
		// 雨伞详情
		umbrellaGroup.Any("/detail", controller.UmbrellaDetailHandler)
		// 雨伞添加
		umbrellaGroup.Any("/add", controller.UmbrellaAddHandler)
		// 雨伞编辑
		umbrellaGroup.Any("/change", controller.UmbrellaChangeHandler)
		// 雨伞删除
		umbrellaGroup.Any("/delete", controller.UmbrellaDeleteHandler)
	}
}

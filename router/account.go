package router

import (
	"gitee.com/dabolau/umbrella/controller"

	"github.com/gin-gonic/gin"
)

// Account 帐号路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func Account(r *gin.Engine) {
	// 帐号分组
	accountGroup := r.Group("/account")
	{
		// 帐号信息
		accountGroup.Any("", controller.AccountHandler)
		// 注册帐号
		accountGroup.Any("/register", controller.RegisterHandler)
		// 登录帐号
		accountGroup.Any("/login", controller.LoginHandler)
		// 找回密码
		accountGroup.Any("/getpassword", controller.GetPasswordHandler)
		// 修改密码
		accountGroup.Any("/changepassword", controller.ChangePasswordHandler)
		// 注销帐号
		accountGroup.Any("/logout", controller.LogoutHandler)
	}
}

package router

import (
	"gitee.com/dabolau/umbrella/controller"
	"gitee.com/dabolau/umbrella/middleware"

	"github.com/gin-gonic/gin"
)

// Bill 票据路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func Bill(r *gin.Engine) {
	// 票据分组
	billGroup := r.Group("/bill")
	// 注册组中间件
	billGroup.Use(middleware.AuthJwt(), middleware.AuthRule())
	{
		// 票据信息
		billGroup.Any("", controller.BillHandler)
		// 票据详情
		billGroup.Any("/detail", controller.BillDetailHandler)
		// 票据添加
		billGroup.Any("/add", controller.BillAddHandler)
		// 票据编辑
		billGroup.Any("/change", controller.BillChangeHandler)
		// 票据删除
		billGroup.Any("/delete", controller.BillDeleteHandler)
	}
}

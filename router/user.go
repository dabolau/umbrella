package router

import (
	"gitee.com/dabolau/umbrella/controller"
	"gitee.com/dabolau/umbrella/middleware"

	"github.com/gin-gonic/gin"
)

// User 用户路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func User(r *gin.Engine) {
	// 用户分组
	userGroup := r.Group("/user")
	// 注册组中间件
	userGroup.Use(middleware.AuthJwt())
	{
		// 用户信息
		userGroup.Any("", middleware.AuthRule(), controller.UserHandler)
		// 用户详情
		userGroup.Any("/detail", middleware.AuthRule(), controller.UserDetailHandler)
		// 用户添加
		userGroup.Any("/add", middleware.AuthRule(), controller.UserAddHandler)
		// 用户编辑
		userGroup.Any("/change", middleware.AuthRule(), controller.UserChangeHandler)
		// 用户删除
		userGroup.Any("/delete", middleware.AuthRule(), controller.UserDeleteHandler)
		// 编辑我的信息
		userGroup.Any("/profile", controller.UserProfileHandler)
	}
}

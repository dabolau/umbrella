package model

import (
	"gorm.io/gorm"
)

// Umbrella 雨伞结构体
// https://gorm.io/zh_CN/docs/models.html
type Umbrella struct {
	gorm.Model                 // 包含了ID, CreatedAt, UpdatedAt, DeletedAt
	UmbrellaName        string `form:"umbrella_name" json:"umbrella_name"`
	UmbrellaTag         string `form:"umbrella_tag" json:"umbrella_tag"`
	UmbrellaDescription string `form:"umbrella_description" json:"umbrella_description"`
	Username            string `form:"username" json:"username"`
	Fullname            string `form:"fullname" json:"fullname"`
	BorrowAt            string `form:"borrow_at" json:"borrow_at"`
	ReturnAt            string `form:"return_at" json:"return_at"`
	DifferenceAt        string `form:"difference_at" json:"difference_at"`
}

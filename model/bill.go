package model

import (
	"gorm.io/gorm"
)

// Bill 票据结构体
// https://gorm.io/zh_CN/docs/models.html
type Bill struct {
	gorm.Model          // 包含了ID, CreatedAt, UpdatedAt, DeletedAt
	Fullname     string `form:"fullname" json:"fullname"`
	UmbrellaName string `form:"umbrella_name" json:"umbrella_name"`
	BorrowAt     string `form:"borrow_at" json:"borrow_at"`
	ReturnAt     string `form:"return_at" json:"return_at"`
	DifferenceAt string `form:"difference_at" json:"difference_at"`
}

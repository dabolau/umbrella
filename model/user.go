package model

import (
	"gorm.io/gorm"
)

// User 用户结构体
// https://gorm.io/zh_CN/docs/models.html
type User struct {
	gorm.Model        // 包含了ID, CreatedAt, UpdatedAt, DeletedAt
	Username   string `form:"username" json:"username"`
	Email      string `form:"email" json:"email"`
	Phone      string `form:"phone" json:"phone"`
	Password   string `form:"password" json:"password"`
	Fullname   string `form:"fullname" json:"fullname"`
	Rule       string `form:"rule" json:"rule"`
}

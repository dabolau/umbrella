package common

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
)

// MD5v1 MD5生成方式1
func MD5v1(s string) (r string) {
	h := md5.New()
	h.Write([]byte(s))
	r = hex.EncodeToString(h.Sum(nil))
	return r
}

// MD5v2 MD5生成方式2
func MD5v2(s string) (r string) {
	data := []byte(s)
	has := md5.Sum(data)
	r = fmt.Sprintf("%x", has)
	return r
}

// MD5v3 MD5生成方式3
func MD5v3(s string) (r string) {
	w := md5.New()
	io.WriteString(w, s)
	r = fmt.Sprintf("%x", w.Sum(nil))
	return r
}

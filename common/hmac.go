package common

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
)

// HMACAndSHA1v1 HMAC（SHA1）生成方式1
func HMACAndSHA1v1(key string, s string) (r string) {
	h := hmac.New(sha1.New, []byte(key))
	h.Write([]byte(s))
	r = hex.EncodeToString(h.Sum(nil))
	return r
}

// HMACAndSHA1v2 HMAC(SHA1)生成方式2
func HMACAndSHA1v2(key string, s string) (r string) {
	w := hmac.New(sha1.New, []byte(key))
	io.WriteString(w, s)
	r = fmt.Sprintf("%x", w.Sum(nil))
	return r
}

// HMACAndMD5v1 HMAC（MD5）生成方式1
func HMACAndMD5v1(key string, s string) (r string) {
	h := hmac.New(md5.New, []byte(key))
	h.Write([]byte(s))
	r = hex.EncodeToString(h.Sum(nil))
	return r
}

// HMACAndMD5v2 HMAC（MD5）生成方式2
func HMACAndMD5v2(key string, s string) (r string) {
	w := hmac.New(md5.New, []byte(key))
	io.WriteString(w, s)
	r = fmt.Sprintf("%x", w.Sum(nil))
	return r
}

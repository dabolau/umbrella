package common

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
)

// SHA1v1 SHA1生成方式1
func SHA1v1(s string) (r string) {
	h := sha1.New()
	h.Write([]byte(s))
	r = hex.EncodeToString(h.Sum(nil))
	return r
}

// SHA1v2 SHA1生成方式2
func SHA1v2(s string) (r string) {
	data := []byte(s)
	has := sha1.Sum(data)
	r = fmt.Sprintf("%x", has)
	return r
}

// SHA1v3 SHA1生成方式3
func SHA1v3(s string) (r string) {
	w := sha1.New()
	io.WriteString(w, s)
	r = fmt.Sprintf("%x", w.Sum(nil))
	return r
}

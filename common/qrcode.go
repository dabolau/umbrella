package common

import (
	"os"
	"path/filepath"

	"github.com/skip2/go-qrcode"
)

// CreateQRCodeFile 创建二维码，成功返回true，失败返回false
func CreateQRCodeFile(content string, fullPathFileName string) bool {
	// 拆分路径和文件名
	paths, _ := filepath.Split(fullPathFileName)
	// 获取文件信息和错误
	_, err := os.Lstat(fullPathFileName)
	// 判断文件是否存在
	isExist := !os.IsNotExist(err)
	if isExist {
		return false
	}
	// 创建拆分后的全路径
	err = os.MkdirAll(paths, os.ModePerm)
	if err != nil {
		return false
	}
	// 创建二维码
	err = qrcode.WriteFile(content, qrcode.Medium, 256, fullPathFileName)
	if err != nil {
		return false
	}
	return true
}

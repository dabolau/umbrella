package common

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

var (
	// FormatYear 年
	FormatYear = "2006"
	// FormatMonth 月
	FormatMonth = "01"
	// FormatDay 日
	FormatDay = "02"
	// FormatHour 时
	FormatHour = "15"
	// FormatMinute 分
	FormatMinute = "04"
	// FormatSecond 秒
	FormatSecond = "05"
	// FormatYearMonthDay 年月日
	FormatYearMonthDay = "2006-01-02"
	// FormatHourMinuteSecond 时分秒
	FormatHourMinuteSecond = "15:04:05"
	// FormatYearMonthDayHourMinuteSecond 年月日时分秒
	FormatYearMonthDayHourMinuteSecond = "2006-01-02 15:04:05"
)

// GetYear 获取年，输出格式“2006”
func GetYear() (date string) {
	date = time.Now().Format("2006")
	return
}

// GetMonth 获取月，输出格式“01”
func GetMonth() (date string) {
	date = time.Now().Format("01")
	return
}

// GetDay 获取日，输出格式“02”
func GetDay() (date string) {
	date = time.Now().Format("02")
	return
}

// GetHour 获取时，输出格式“15”
func GetHour() (date string) {
	date = time.Now().Format("15")
	return
}

// GetMinute 获取分，输出格式“04”
func GetMinute() (date string) {
	date = time.Now().Format("04")
	return
}

// GetSecond 获取秒，输出格式“05”
func GetSecond() (date string) {
	date = time.Now().Format("05")
	return
}

// GetYearMonthDay 获取年月日，输出格式“2006-01-02”
func GetYearMonthDay() (date string) {
	date = time.Now().Format("2006-01-02")
	return
}

// GetHourMinuteSecond 获取时分秒，输出格式“15:04:05”
func GetHourMinuteSecond() (date string) {
	date = time.Now().Format("15:04:05")
	return
}

// GetYearMonthDayHourMinuteSecond 获取年月日时分秒，输出格式“2006-01-02 15:04:05”
func GetYearMonthDayHourMinuteSecond() (date string) {
	date = time.Now().Format("2006-01-02 15:04:05")
	return
}

// GetHoursDifference 获取时差，输出格式“1”时
func GetHoursDifference(t1String string, t2String string) (date string) {
	t1, _ := time.ParseInLocation("2006-01-02 15:04:05", t1String, time.Local)
	t2, _ := time.ParseInLocation("2006-01-02 15:04:05", t2String, time.Local)
	dateValue := t2.Unix() - t1.Unix()
	date = strconv.FormatInt(dateValue/3600, 10)
	return
}

// GetMinutesDifference 获取分差，输出格式“1”分
func GetMinutesDifference(t1String string, t2String string) (date string) {
	t1, _ := time.ParseInLocation("2006-01-02 15:04:05", t1String, time.Local)
	t2, _ := time.ParseInLocation("2006-01-02 15:04:05", t2String, time.Local)
	dateValue := t2.Unix() - t1.Unix()
	date = strconv.FormatInt(dateValue/60, 10)
	return
}

// GetSecondsDifference 获取秒差，输出格式“1”秒
func GetSecondsDifference(t1String string, t2String string) (date string) {
	t1, _ := time.ParseInLocation("2006-01-02 15:04:05", t1String, time.Local)
	t2, _ := time.ParseInLocation("2006-01-02 15:04:05", t2String, time.Local)
	dateValue := t2.Unix() - t1.Unix()
	date = strconv.FormatInt(dateValue, 10)
	return
}

// GetHourMinuteSecondDifference 获取时分秒差，输出格式“1时1分1秒”
func GetHourMinuteSecondDifference(t1String string, t2String string) (date string) {
	t1, _ := time.ParseInLocation("2006-01-02 15:04:05", t1String, time.Local)
	t2, _ := time.ParseInLocation("2006-01-02 15:04:05", t2String, time.Local)
	dateValue := t2.Sub(t1)
	date = fmt.Sprintf("%v", dateValue)
	date = strings.ReplaceAll(date, "h", "时")
	date = strings.ReplaceAll(date, "m", "分")
	date = strings.ReplaceAll(date, "s", "秒")
	return
}

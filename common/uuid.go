package common

import "github.com/google/uuid"

// UUIDv1 基于时间的编号（唯一）
func UUIDv1() string {
	id, _ := uuid.NewUUID()
	return id.String()
}

// UUIDv2 基于分布式计算环境的编号（安全）
func UUIDv2() string {
	id, _ := uuid.NewDCEGroup()
	return id.String()
}

// UUIDv3 基于命名空间的编号（MD5）
func UUIDv3() string {
	dcePerson, _ := uuid.NewDCEPerson()
	id := uuid.NewMD5(dcePerson, []byte("abcdefghigklmnopqrstuvwxyz"))
	return id.String()
}

// UUIDv4 基于随机数的编号（唯一）
func UUIDv4() string {
	id, _ := uuid.NewRandom()
	return id.String()
}

// UUIDv5 基于命名空间的编号（SHA1）
func UUIDv5() string {
	dcePerson, _ := uuid.NewDCEPerson()
	id := uuid.NewSHA1(dcePerson, []byte("abcdefghigklmnopqrstuvwxyz"))
	return id.String()
}

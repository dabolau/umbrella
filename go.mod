module gitee.com/dabolau/umbrella

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.2
	github.com/jordan-wright/email v4.0.1-0.20200917010138-e1c00e156980+incompatible
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	gorm.io/driver/mysql v1.0.3
	gorm.io/driver/sqlite v1.1.3
	gorm.io/gorm v1.20.5
)

package database

import (
	"gorm.io/gorm"
)

var (
	// DB 数据库的全局变量方便后期调用
	DB *gorm.DB
)

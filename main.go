package main

import (
	"html/template"

	"gitee.com/dabolau/umbrella/database"
	"gitee.com/dabolau/umbrella/function"
	"gitee.com/dabolau/umbrella/middleware"
	"gitee.com/dabolau/umbrella/model"
	"gitee.com/dabolau/umbrella/router"

	"github.com/gin-gonic/gin"
)

// 函数入口
// https://gin-gonic.com
func main() {
	// 数据库连接
	database.OpenSQLite3()
	// 数据库迁移
	database.DB.AutoMigrate(&model.User{})
	database.DB.AutoMigrate(&model.Bill{})
	database.DB.AutoMigrate(&model.Umbrella{})
	// 定义默认路由
	r := gin.Default()
	// 注册全局中间件
	// https://gin-gonic.com/docs/examples/using-middleware/
	r.Use(middleware.StatCost())
	// 自定义模板函数
	// https://gin-gonic.com/docs/examples/html-rendering/
	r.SetFuncMap(template.FuncMap{
		"truncateChars": function.TruncateChars,
		"formatAsDate":  function.FormatAsDate,
		"safeHTML":      function.SafeHTML,
	})
	// 设置表单上传大小（默认为32M）
	r.MaxMultipartMemory = 1024
	// 定义静态文件路径
	r.Static("/static", "./static")
	// 定义模板路径
	r.LoadHTMLGlob("template/**/*")
	// 加载路由
	router.Home(r)
	router.Account(r)
	router.User(r)
	router.Bill(r)
	router.Umbrella(r)
	// 启动服务
	r.Run("0.0.0.0:8081")
}

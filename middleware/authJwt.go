package middleware

import (
	"log"
	"net/http"
	"time"

	"gitee.com/dabolau/umbrella/common"
	"gitee.com/dabolau/umbrella/database"
	"gitee.com/dabolau/umbrella/model"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// JwtClaims 结构体
type JwtClaims struct {
	Username           string `json:"username"`
	Password           string `json:"password"`
	jwt.StandardClaims        // jwt包中的默认字段
}

var (
	// 过期时间
	_jwtExpireTime = time.Second * (30 * 86400) // 30*86400=30天
	// 签发人
	_jwtIssuer = "dablau"
	// 密钥
	_jwtSecret = []byte("春花秋月何时了")
)

// AuthJwt 验证授权中间件
// https://gin-gonic.com/docs/examples/custom-middleware/
// https://gin-gonic.com/docs/examples/cookie/
// https://github.com/dgrijalva/jwt-go
// 登录成功 设置Jwt
// jwtToken, _ := middleware.GenerateJwt(account.Username, common.DecodeBase64(account.Password))
// middleware.SetJwt(c, "jwtToken", jwtToken)
// 注销成功 删除Jwt
// middleware.DeleteJwt(c, "jwtToken")
func AuthJwt() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Println("验证授权中间件")
		var account model.User
		// 获取Jwt
		jwtToken := GetJwt(c, "jwtToken")
		// 授权失败，获取到的令牌数据为空
		if jwtToken == nil {
			// 不调用该请求的剩余处理程序
			c.Abort()
			// 页面跳转
			c.Redirect(http.StatusFound, "/account/login")
			return
		}
		// 解析Jwt
		claims, err := ParseJwt(jwtToken.(string)) //解析令牌
		// 授权失败，解析到的数据错误
		if err != nil {
			// 不调用该请求的剩余处理程序
			c.Abort()
			// 页面跳转
			c.Redirect(http.StatusFound, "/account/login")
			return
		}
		// 查询数据
		database.DB.Where(&model.User{
			Username: claims.Username,
			Password: common.EncodeBase64(claims.Password),
		}).First(&account)
		// 授权失败，未查询到数据
		if account.ID < 1 {
			// 不调用该请求的剩余处理程序
			c.Abort()
			// 页面跳转
			c.Redirect(http.StatusFound, "/account/login")
			return
		}
		// 授权成功
		// 在请求上下文中设置值，后续的处理函数能够取到该值
		c.Set("username", account.Username)
		c.Set("email", account.Email)
		c.Set("phone", account.Phone)
		c.Set("password", common.DecodeBase64(account.Password))
		c.Set("fullname", account.Fullname)
		c.Set("rule", account.Rule)
		// 调用该请求的剩余处理程序
		c.Next()
	}
}

// GenerateJwt 生成Jwt
// https://godoc.org/github.com/dgrijalva/jwt-go#NewWithClaims
func GenerateJwt(username string, password string) (string, error) {
	// 自定义声明
	jwtClaims := JwtClaims{
		Username: username, // 自定义字段
		Password: password,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(_jwtExpireTime).Unix(), // 过期时间
			Issuer:    _jwtIssuer,                            // 签发人
		},
	}
	// 使用指定的签名方法创建签名对象
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtClaims)
	// 使用指定的密钥（_jwtSecret）签名并获得完整的编码后的字符串token
	tokenString, err := token.SignedString(_jwtSecret)
	return tokenString, err
}

// ParseJwt 解析Jwt
// https://godoc.org/github.com/dgrijalva/jwt-go#ParseWithClaims
func ParseJwt(tokenString string) (*JwtClaims, error) {
	// 解析令牌
	token, err := jwt.ParseWithClaims(tokenString, &JwtClaims{}, func(token *jwt.Token) (interface{}, error) {
		return _jwtSecret, nil
	})
	// 解析声明
	jwtClaims, ok := token.Claims.(*JwtClaims)
	// 校验令牌
	if ok && token.Valid {
		return jwtClaims, nil
	}
	return nil, err
}

// SetJwt 设置Jwt
func SetJwt(c *gin.Context, tokenKey interface{}, tokenValue interface{}) {
	c.SetCookie(tokenKey.(string), tokenValue.(string), 30*86400, "/", c.Request.Host, false, true) // 30*86400=30天
}

// GetJwt 获取Jwt
func GetJwt(c *gin.Context, tokenKey interface{}) (tokenValue interface{}) {
	tokenValue, err := c.Cookie(tokenKey.(string))
	if err != nil {
		return nil
	}
	return tokenValue
}

// DeleteJwt 删除Jwt
func DeleteJwt(c *gin.Context, tokenKey interface{}) {
	c.SetCookie(tokenKey.(string), "", -1, "/", c.Request.Host, false, true)
}

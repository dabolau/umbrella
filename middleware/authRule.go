package middleware

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

// AuthRule 验证规则中间件
// https://gin-gonic.com/docs/examples/custom-middleware/
func AuthRule() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Println("验证规则中间件")
		// 在请求上下文中获取值，需在中间件中设置
		rule := c.MustGet("rule").(string)
		log.Println(rule)
		if rule == "管理员" {
			// 调用该请求的剩余处理程序
			c.Next()
			return
		}
		// 不调用该请求的剩余处理程序
		c.Abort()
		// 页面跳转
		c.Redirect(http.StatusFound, "/index")
		return
	}
}

package middleware

import (
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

// StatCost 请求耗时中间件
// https://gin-gonic.com/docs/examples/custom-middleware/
func StatCost() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		// 调用该请求的剩余处理程序
		c.Next()
		// 不调用该请求的剩余处理程序
		// c.Abort()
		// 计算耗时
		cost := time.Since(start)
		log.Println(cost)
	}
}

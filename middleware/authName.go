package middleware

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

// AuthName 验证姓名中间件
// https://gin-gonic.com/docs/examples/custom-middleware/
func AuthName() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Println("验证姓名中间件")
		// 在请求上下文中获取值，需在中间件中设置
		fullname := c.MustGet("fullname").(string)
		log.Println(fullname)
		if fullname == "" {
			// 不调用该请求的剩余处理程序
			c.Abort()
			// 页面跳转
			c.Redirect(http.StatusFound, "/user/profile")
			return
		}
	}
}

package function

import "html/template"

// SafeHTML 超文本标记语言模板函数
// https://gin-gonic.com/docs/examples/html-rendering/
func SafeHTML(s string) template.HTML {
	return template.HTML(s)
}

package function

import (
	"fmt"
	"time"
)

// FormatAsDate 格式化日期模板函数
// https://gin-gonic.com/docs/examples/html-rendering/
func FormatAsDate(t time.Time) string {
	year, month, day := t.Date()
	return fmt.Sprintf("%d-%02d-%02d", year, month, day)
}

package function

import "fmt"

// TruncateChars 截断字符模板函数
// https://gin-gonic.com/docs/examples/html-rendering/
func TruncateChars(s string) string {
	i := 20
	if len([]rune(s)) > i {
		return fmt.Sprintf("%s...", string([]rune(s)[0:i]))
	}
	return string([]rune(s))
}
